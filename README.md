# Overview
Ever since I got Phillips hue lights set up, I was always a bit disappointed with the widget supplied by Phillips. Now that I've got free time on my hands, I decided to have a look at KWGT and Tasker to see if I could create a better widget.

It should be said that this is the first time I've used either of these apps, so I expect there will be some things that could be done a lot better. I'm open to feedback and requests/suggestions/fixes.

This widget tries to match the color of the room dynamically, and handles out-of-widget lighting changes (Google home, hue, etc) and is very configurable both in KWGT and Tasker.

![](https://media.giphy.com/media/KGMBTSHsakrU9Z0toR/giphy.gif)

# Technical Requirements
- KWGT (Pro)
- Tasker
- AutoHue


# Installation
So, this widget took a fair amount of Tasker integration to get working. Tasker is used for a lot of the heavy lifting.
During the setup of this widget, you'll need to be able to go inside of each task and configure any AutoHue tasks to point to your bridge and lights.

Other than that, everything should work as expected right away (assuming you still have to default hue scenes).

### Tasker
Unfortunately, the Tasker installation is a little more involved. First, [import the project](https://taskernet.com/shares/?user=AS35m8nlcUcqKCfVawUeMlUPsuuiXj1RC7bPA8RvNPFExH7GjnOP6Xgv2%2FKnOWqQJV4sP34lXRg%3D&id=Project%3AHUE_KWGT_WIDGET).

Then, you'll have to go through each AutoHue plugin call, and select your own bridge IP and light groups. The first time you do this, you'll have to create a new bridge.
There are AutoHue calls (nominated as `HueState` or `Hue 2.0` within tasks) inside of every task you imported.

1. Go through each task you imported
2. Find any instance of `HueState` or `Hue 2.0` and tap on it
3. Tap the edit (pencil) button in the upper right, next to 'Configuration'
4. If this is first time setup, press the plus button to register a new bridge
5. Select your bridge from the dropdown
6. Select a light (Hue State), or a lighting group to target (Hue 2.0).
7. If you're editing a `KWGT_SCENE_X` task (where X is 1-5), then you should also configure how you'd like the lights to be set. It's intended that you use 'Set Scene', however you don't have to.

**Important Note:** The `Hue 2.0` tasks _must_ have a lighting group selected, and not single lights. This is because the widget activates _scenes_, which are only available when a group is selected.

### KWGT
After running the Tasker import above, the KWGT widget should have automatically been installed by the SETUP task to `/Kustom/widgets/`.
Simply import this widget using `KWGT > Load Preset > Exported` and adjust the globals as you see fit.

# Configuration
This widget has been made fairly simple to adjust. Most simple things should be easy to spot from inside the globals section. For more common and/or complicated changes, see the below sections.

### Change a scene name (or image)
If you want to change the name of a scene as it's displayed inside of the widget, simply change the `sx_name` global, where `x` is the scene number (1-5).

Follow similar steps for changing the image (sx_image).

### Changing what scene is activated
To change which scene is activated when the widget is interacted with, you'll have to adjust the scene target from inside of Tasker; the widget actually has no bearing on what scene is triggered.

First, find which scene button you're trying to adjust. For instance, in the default widget changing the Concentrate button would mean I'm changing _scene 3_. The scenes are labelled 1-5, left to right.

Next, head to Tasker and select the `KWGT_SCENE_X` where `X` is the scene number found in the previous step.

Open the Hue 2.0 task, tap edit, and scroll to 'Set Scene'. Select your new scene here and go try out the widget.

_It may flicker with the wrong color the first time due to the cacheing of the scene colours._


# Tasker Documentation
The following is a technical description of each profile, task and variable.

## [Global Var] %LAUNCHER_NAME
The name of your desired launcher (e.g. Nova Launcher). Used by the KWGT_UPDATE task to force a widget update when the user switches
to their launcher.

## \[Global Var] [Auto Generated] %HUE_CL
The current colour of the hue lights, as a hexadecimal value. Should not need to be altered, automatically created and populated.

## \[Global Var] [Auto Generated] %HUE_STATUS
The current status of the hue lights, as a boolean value. Should not need to be altered, automatically created and populated.

## \[Global Var] [Auto Generated] %SCENE\_X\_CACHE
Where `X` is 1-5, contains the hexadecimal representation of the scenes colour for speed. This cached version is first by `KWGT_UPDATE`. Updated every time the scene
is swapped to.

## [Profile] KWGT Auto Update
This profile is activated each time the user navigates to their launcher. When activated, the KWGT_UPDATE task is executed.
Using this profile ensures that the widget represents the state of the Hue lights even when controlled from outside the widget (Hue app, Google home, etc).

*Not working? Ensure you've set the `%LAUNCHER_NAME` global var to the name of your launcher*

## [Task] KWGT\_UPDATE
This task is responsible for fetching the current state of the Hue lights and updating the `%HUE_STATUS` and `%HUE_CL` global vars.
It depends on the `Tasker/js/hsl_rgb_converter2.js` script to convert the Hue CIE color information to RGB (hexadecimal).

If this task is called by a `KWGT_SCENE_X` task, the scene `X` will have it's color value cached automatically (`%SCENE_X_CACHE`). The next time this Task is run by a `KWGT_SCENE_X` task, the cached
value will be used immediately, and then replaced with the real value some time later after the bridge requests are completed.
_This can result in a flicker if the scene preset colors change._

## [Task] KWGT\_SCENE\_X
Where X is 1-5, this task is called by the KWGT widget to force a scene change. This is why there are 5 tasks, one for each scene preset displayed on the widget.

These scenes are very basic; they simply change the scene using AutoHue, and then call the `KWGT_UPDATE` (passing the scene being switched to for cacheing).

## [Task] KWGT\_TOGGLE
A simple task which simply inverts the `%HUE_STATUS` var, broadcasts the new value to the KWGT widget, and uses AutoHue to change the light status to match.